# distcc-docker-imagesi

### _distcc in docker with multi-distro support_
 [![forthebadge](https://forthebadge.com/images/badges/built-with-love.svg)](https://forthebadge.com) [![forthebadge](https://forthebadge.com/images/badges/powered-by-jeffs-keyboard.svg)](https://forthebadge.com) [![forthebadge](https://forthebadge.com/images/badges/contains-cat-gifs.svg)](https://forthebadge.com)

[![distcc-docker-images](https://github.com/Bensuperpc/distcc-docker-images/actions/workflows/main.yml/badge.svg)](https://github.com/Bensuperpc/distcc-docker-images/actions/workflows/main.yml)

# New Features !

  - Multi-build (Ubuntu, debian, fedora, alpine)

#### Install
You need Linux distribution like Ubuntu or Manjaoro

```sh
https://github.com/Bensuperpc/distcc-docker-images.git
```
```sh
cd distcc-docker-images
```
#### Usage

Optional: Pull docker image from: https://hub.docker.com/repository/docker/bensuperpc/distcc

```sh
./run.sh && ./test.sh (It may take time)
```
#### Build
```sh
docker build . -t bensuperpc/distcc -f Dockerfile_fedora
```

### Todos

 - Write Tests
 - Continue dev. :D

### More info : 
- https://developers.redhat.com/blog/2019/05/15/2-tips-to-make-your-c-projects-compile-3-times-faster/
- https://link.medium.com/cfhBeb298V

License
----

MIT License


**Free Software forever !**
   
 
